<?php

include_once './inc/class-session-shell.php';

$custom_sess = new SessionShell;

// Окончательно удаляем данные
if( empty( $custom_sess->get('email_msg') ) && empty( $custom_sess->get('amo_crm_msg') ) ) {
    $custom_sess->destroy();
}

?>
<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <title>Тестовое задание от претендента Гистоловского А.А.</title>
        <link rel="icon" href="favicon.ico">
        <link rel="preload" as="font" href="./assets/css/font.css">
        <link rel="stylesheet" href="./assets/css/style.css">
    </head>

    <body>
        <header>
            <h1>Тестовое задание</h1>
        </header>

        <main id="main">
            <a id="main_button" class="main_button" href="#" style="width: 235px; height: 70px;">Кликни</a>
        </main>

        <footer id="colophon" class="site-footer" role="contentinfo">
            <ul class="contacts">
                <li><strong>&#169;</strong>2022 <a class="resume_link" href="https://disk.yandex.ru/i/4PlzUkGvDSoaIw" title="Резюме претендента на вакансию">Гистоловский А.А.</a></li>
            </ul>
        </footer><!-- #colophon -->

        <div id="popup" class="popup hidden">
            <div class="popup_wrapper">
                <div class="column_wrapper">
                    <div class="col-left">
                        <div class="popup_title">Получите набор файлов для руководителя:</div>
                        <div class="image_wrapper">
                            <div class="docs_images">
                                <img src="./assets/images/docs_image_doc.png" alt="Иконка загруженного документа doc">
                                <img src="./assets/images/docs_image_xls.png" alt="Иконка загруженного документа xls">
                                <img src="./assets/images/docs_image_pdf.png" alt="Иконка загруженного документа pdf">
                                <img src="./assets/images/docs_image_pdf.png" alt="Иконка загруженного документа pdf">
                                <img src="./assets/images/docs_image_pdf.png" alt="Иконка загруженного документа pdf">
                                <img src="./assets/images/docs_image_pdf.png" alt="Иконка загруженного документа pdf">
                                <img src="./assets/images/docs_image_pdf.png" alt="Иконка загруженного документа pdf">
                            </div>
                            <div class="present_images">
                                <img src="./assets/images/presentation-image.png" alt="Файлы для руководителя">
                            </div>
                        </div>
                    </div>
                    <div class="popup_content">
                        <form id="popup_form" action="./inc/class-test-sendmail.php" method="post">
                            <input id="site" type="hidden" name="site" value="">
                            <div class="string">
                                <label>
                                    Введите Email для получения файлов:
                                    <input id="email" type="email" name="email" placeholder="E-mail" value="">
                                </label>
                            </div>
                            <div class="string">
                                <label>
                                    Введите телефон для подтверждения доступа:
                                    <input id="phone-mask" name="phone" type="tel" placeholder="+7 (000) 000-00-00" required
                                           data-mask="+7 (000) 000-00-00"
                                           pattern="\+7 \([0-9]{3}\) [0-9]{3}[\-][0-9]{2}[\-][0-9]{2}" value="">
                                </label>
                            </div>
                            <div class="string">
                                <button id="submit_btn" class="submit_btn" type="submit" aria-label="Submit button">
                                    Скачать файлы
                                    <img src="./assets/images/btn_hand.png" alt="Submit icon">
                                </button>
                                <div class="files_wrapper">
                                    <span class="file">
                                        <span class="file_format">PDF</span>
                                        <span class="file_size">4,7 MB</span>
                                    </span>
                                    <span class="file">
                                        <span class="file_format">DOC</span>
                                        <span class="file_size">0,8 MB</span>
                                    </span>
                                    <span class="file">
                                        <span class="file_format">XLS</span>
                                        <span class="file_size">1,2 MB</span>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="close_btn" class="close_btn"></div>
            </div>
        </div>

        <div id="notifications" class="notifications"></div>

        <script type="text/javascript">
            // Передаем ответ от функционала отправки писем
            const email_msg = '<?php echo $custom_sess->get( 'email_msg' ); ?>';

            // Передаем ответ от функционала отправки данных в AmoCRM
            const amo_crm_msg = '<?php echo $custom_sess->get('amo_crm_msg'); ?>';

            show_msg(amo_crm_msg);
            show_msg(email_msg);
        </script>
        <?php
        // Очищаем сообщения из сессии
        $custom_sess->set('email_msg', '');
        $custom_sess->set('amo_crm_msg', '');
        ?>
        <script type="text/javascript" src="./assets/js/script.js"></script>
    </body>
</html>
