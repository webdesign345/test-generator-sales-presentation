<?php

include_once 'class-session-shell.php';
include_once 'class-amo-crm.php';

class TestSendmail {

    private $to = '<info@example.com>';
    private $from = '<test@example.com>';
    private $phone_san;
    private $email_san;
    private $subject_dafault = 'Заявка от пользователя';
    private $mail_body_title = 'Заявка на получение набора файлов для руководителей';
    private $mail_body_contact_title = 'Контакты заявителя:';
    private $mail_body_table_head_phone = 'Номер телефона';
    private $mail_body_table_head_email = 'E-mail';
    private $subject_san;
    private $message;
    private $header;
    private $send_email_responce_msg_success = 'Письмо успешно отправлено';
    private $send_email_responce_msg_error = 'Ошибка отправки письма';
    private $send_data_to_crm_responce_msg_success = 'Данные успешно отправлены в AmoCRM';
    private $send_data_to_crm_responce_msg_error = 'Ошибка отправки данных в AmoCRM';

    public function __construct($user_phone, $user_email, $email_to=null, $subject=null) {

        $this->phone_san = filter_var( $user_phone, FILTER_SANITIZE_URL);
        $this->email_san = ( isset($user_email) && !empty($user_email) ) ? filter_var( $user_email, FILTER_SANITIZE_EMAIL) : '-';
        $this->subject_san = ( $subject ) ? filter_var( $subject, FILTER_SANITIZE_STRING) : $this -> subject_dafault;

        if($email_to) {
            $this->to.= ', ' . $email_to;
        }

        $this->message = '<html>
                        <head>
                          <title>' . $this->mail_body_title . '</title>
                        </head>
                        <body>
                          <p>' . $this->mail_body_contact_title . '</p>
                          <table>
                            <tr>
                              <th>' . $this->mail_body_table_head_phone . '</th><th>' . $this->mail_body_table_head_email . '</th>
                            </tr>
                            <tr>
                              <td>' . $this->phone_san . '</td><td>' . $this->email_san . '</td>
                            </tr>
                          </table>
                        </body>
                    </html>';

        $this->headers  = 'MIME-Version: 1.0' . "\r\n";
        $this->headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $this->headers .= 'From: ' . $this->from . "\r\n";

    }

    public function get_mail() {
        return mail($this->to, $this->subject_san, $this->message, $this->headers);
    }
}

if( $_POST['phone'] ) {
    $send_email = new TestSendmail($_POST['phone'], $_POST['email'] );

    $email_msg = '';
    if( $send_email->get_mail() ) {
        $email_msg = $this->send_email_responce_msg_success;
    } else {
        $email_msg = $this->send_email_responce_msg_error;
    }

    $custom_sess = new SessionShell;
    $custom_sess->set('email_msg', $email_msg);

    $amo_crm = new AmoCRM;
    $send_data_amo_crm = $amo_crm->add_dial( $_POST['phone'], $_POST['email'] );

    $amo_crm_msg = '';
    if($send_data_amo_crm[0]) {
        if( $send_data_amo_crm[0]['id'] ) {
            $amo_crm_msg = $this->send_data_to_crm_responce_msg_success;
        } else {
            $amo_crm_msg = $this->send_data_to_crm_responce_msg_error;
        }
    }

    if( $send_data_amo_crm['error_msg'] ) {
        $amo_crm_msg = $send_data_amo_crm['error_msg'] . ', ' . $send_data_amo_crm['error_code'];
    }

    $custom_sess->set('amo_crm_msg', $amo_crm_msg);

    header('Location: ' . $_POST['site']);
    exit;
}
