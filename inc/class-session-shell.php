<?php

class SessionShell {

    public function __construct() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function set($name, $value) {
        $_SESSION["$name"]=$value;
    }

    public function get($name) {
        return $_SESSION["$name"];
    }

    public function del($name) {
        unset($_SESSION["$name"]);
    }

    public function exists($name) {
        return ($_SESSION["$name"]) ? true : false;
    }

    public function destroy() {
        // Удаляем все переменные сессии.
        $_SESSION = [];

        // Если требуется уничтожить сессию, также необходимо удалить сессионные cookie.
        // Замечание: Это уничтожит сессию, а не только данные сессии!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();

            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }

        // Наконец, уничтожаем сессию.
        session_destroy();
    }
}
