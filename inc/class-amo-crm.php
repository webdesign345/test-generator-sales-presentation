<?php

class AmoCRM {

    // поддомен AmoCRM можно найти в адресной строке личного кабинета - https://ххххххххх.amocrm.ru/
    private $amo_account = 'ххххххххх';

    // Redirect url, в данном функционале не используется, но указать его нужно точно такой же как при создании интеграции
    private $redirect_uri = 'http://example.com/';

    // Файл для сохранения ключей, по умолчанию отсутствует, после первого обращения будет создан
    private $token_file = 'amo-tokens.txt';

    // ID берем из параметров созданной нами интеграции
    private $client_id = '';

    // Секретный ключ берем из параметров созданной нами интеграции
    private $client_secret = '';

    /**
     * Код авторизации auth_token действителен 20 минут, после чего нужно сгенерировать заново
     * Код авторизации будет доступен при создании интеграции на странице https://ххххххххх.amocrm.ru/settings/widgets/
     * на вкладке "Ключи и доступы"
     * За 20 минут нужно получить access_token и refresh_token в get_access_token, данные автоматически сохранятся
     * в файле - $token_file
     */
    private $auth_code = '';

    private $access_token;
    private $refresh_token;
    private $token_type;
    private $expires_in;
    private $endTokenTime;

    private $dail_name = 'сделка ХХХ';
    private $contact_name = 'ale';

    public function __construct() {
        $tokens = json_decode( file_get_contents ( $this -> token_file ), true );

        if($tokens) {
            $this->access_token = $tokens['access_token'];
            $this->refresh_token = $tokens['refresh_token'];
            $this->token_type = $tokens['token_type'];
            $this->expires_in = $tokens['expires_in'];
            $this->endTokenTime = $tokens['endTokenTime'];
        }

        // Получаем access_token в первый раз
        if( $this->auth_code ) {
            $this->get_access_token('authorization_code');

        // Обновляем access_token если его срок(1 сутки с момента получения) уже истек
        }else if( $tokens && $this->endTokenTime < time() ) {
            $this->get_access_token( 'refresh_token' );
        }
    }

    // Основной функционал передачи параметров в AmoCRM
    public function add_dial($phone, $email) {

        // Санитизация поля телефона
        $phone_san = filter_var( $phone, FILTER_SANITIZE_URL);

        // Санитизация поля e-mail
        $email_san = ( isset( $email ) && !empty( $email ) ) ? filter_var( $email, FILTER_SANITIZE_EMAIL) : '';

        $json_data = '[
                        {
                            "name": "' . $this->dail_name . '",
                            "_embedded": {
                                "contacts": [
                                    {
                                        "name": "' . $this->contact_name . '",
                                        "custom_fields_values": [
                                            {
                                                "field_code": "EMAIL",
                                                "values": [
                                                    {
                                                        "value": "' . $email_san . '"
                                                    }
                                                ]
                                            },
                                            {
                                                "field_code": "PHONE",
                                                "values": [
                                                    {
                                                        "value": "' . $phone_san . '"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ]';

        $curl = curl_init();

        curl_setopt_array( $curl, array(
            CURLOPT_URL => 'https://' . $this->amo_account . '.amocrm.ru/api/v4/leads/complex',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $json_data,
            CURLOPT_HTTPHEADER => array(
                'User-Agent: amoCRM-oAuth-client/1.0',
                'Authorization: Bearer ' . $this->access_token,
                'Content-Type: application/json'
            ),
        ) );

        $amo_resp = curl_exec( $curl );
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        // Проверяет дополнительно, проходит у нас авторизация или нет
        // При необходимости запускаем получение нового токена
        $this->check_response_error( json_decode( $amo_resp, true ) );

        // Вывод данных в лог
//        $fileOpen = fopen( __DIR__ . '/add_dial.log', 'a');
//        if($fileOpen) {
//            $fileWrite = fwrite ( $fileOpen, '*************************** Log to File ****************' . PHP_EOL);
//            if($fileWrite){
//                fwrite ( $fileOpen, 'date - ' . date('d M Y H:i:s') . PHP_EOL);
//                fwrite ( $fileOpen, '$phone_san - ' . serialize($phone_san) . PHP_EOL);
//                fwrite ( $fileOpen, '$email_san - ' . serialize($email_san) . PHP_EOL);
//                fwrite ( $fileOpen, '$json_data - ' . serialize( $json_data ) . PHP_EOL);
//                fwrite ( $fileOpen, '$amo_resp - ' . serialize( $amo_resp ) . PHP_EOL);
//                fwrite ( $fileOpen, '$code - ' . serialize( $code ) . PHP_EOL);
//                fclose ( $fileOpen );
//            } else {
//                echo '<script>alert("Error for writing file");</script>';
//            }
//
//        } else {
//            echo '<script>alert("Error for opening file");</script>';
//        }

        $code = (int)$code;
        $errors = [
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];

        try {
            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
            }
        } catch(Exception $e) {
            return array('error_msg' => 'Ошибка: ' . $e->getMessage(), 'error_code' => 'Код ошибки: ' . $e->getCode() );
        }

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        return json_decode($amo_resp, true);
    }

    // Функционал проверки ответа на ошибку доступа
    // Если есть ошибка, то автоматически запустится получение нового токена авторизации
    protected function check_response_error( $resp_arr ) {
        // Вывод данных в лог
//        $fileOpen = fopen( __DIR__ . '/check_response_error.log', 'a');
//        if($fileOpen) {
//            $fileWrite = fwrite ( $fileOpen, '*************************** Log to File ****************' . PHP_EOL);
//            if($fileWrite){
//                fwrite ( $fileOpen, 'date - ' . date('d M Y H:i:s') . PHP_EOL);
//                fwrite ( $fileOpen, '$resp_arr - ' . serialize($resp_arr) . PHP_EOL);
//                fclose ( $fileOpen );
//            } else {
//                echo '<script>alert("Error for writing file");</script>';
//            }
//
//        } else {
//            echo '<script>alert("Error for opening file");</script>';
//        }

        if( 401 === $resp_arr['status']) {
            $this->get_access_token( 'refresh_token' );
        }
    }

    // Функционал получения auth_token по коду авторизации и смене auth_token по refresh_token после 24 часов

    /**
     * @param $grant_type string - Может принимать 2 варианта значений (1 - authorization_code - используется первый
     *                              раз для получения токена авторизации, 2 - refresh_token - означает что текущий
     *                              токен авторизации стал не действительным и его нужно обновить)
     */
    protected function get_access_token( $grant_type ) {
        $curl = curl_init();

        $post_fields = array(
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'grant_type' => $grant_type,
            'redirect_uri' => $this->redirect_uri
        );

        if( 'authorization_code' === $grant_type ) {
            $post_fields['code'] = $this->auth_code;
        }

        if( 'refresh_token' === $grant_type ) {
            $post_fields['refresh_token'] = $this -> refresh_token;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'. $this->amo_account . '.amocrm.ru/oauth2/access_token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_HTTPHEADER => array(
                'User-Agent: amoCRM-oAuth-client/1.0'
            ),
        ));

        $amo_resp = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $code = (int)$code;
        $errors = [
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];

        try {
            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
            }
        }
        catch(Exception $e) {
            die('Ошибка: ' . $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode());
        }

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        $response = json_decode($amo_resp, true);

        $arrParamsAmo = [
            "access_token"  => $response['access_token'],
            "refresh_token" => $response['refresh_token'],
            "token_type"    => $response['token_type'],
            "expires_in"    => $response['expires_in'],
            "endTokenTime"  => $response['expires_in'] + time(),
        ];

        $arrParamsAmo = json_encode($arrParamsAmo);

        // Если файл существует - удаляем его
        if(file_exists($this -> token_file)) {
            unlink($this -> token_file);
        }

        // Вывод данных в лог
//        $fileOpen = fopen( __DIR__ . '/get_access_token.log', 'a');
//        if($fileOpen) {
//            $fileWrite = fwrite ( $fileOpen, '*************************** Log to File ****************' . PHP_EOL);
//            if($fileWrite){
//                fwrite ( $fileOpen, 'date - ' . date('d M Y H:i:s') . PHP_EOL);
//                fwrite ( $fileOpen, '$resp_arr - ' . $arrParamsAmo . PHP_EOL);
//                fwrite ( $fileOpen, 'Update token file - ' . PHP_EOL);
//                fclose ( $fileOpen );
//            } else {
//                echo '<script>alert("Error for writing file");</script>';
//            }
//
//        } else {
//            echo '<script>alert("Error for opening file");</script>';
//        }

        // Создаем новый файл с новыми ключами доступа
        $f = fopen($this->token_file, 'w');
        fwrite($f, $arrParamsAmo);
        fclose($f);
    }
}
