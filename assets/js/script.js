
function popup_fn( el, param ) {
    const popup_el = el;
    let hidden_class = '';
    let active_class = '';

    if( 'open' === param ) {
        if(popup_el.classList.contains('hidden')) {
            hidden_class = 'hidden';
            active_class = 'active';
        }
    } else {
        if(popup_el.classList.contains('active')) {
            hidden_class = 'active';
            active_class = 'hidden';
        }
    }

    popup_el.classList.remove(hidden_class);
    popup_el.classList.add(active_class);

}

function maskInput() {
    var input = this;
    var mask = input.dataset.mask;
    var value = input.value;
    var literalPattern = /[0\*]/;
    var numberPattern = /[0-9]/;
    var newValue = "";
    try {
        var maskLength = mask.length;
        var valueIndex = 0;
        var maskIndex = 0;

        for (; maskIndex < maskLength;) {
            if (maskIndex >= value.length) break;

            if (mask[maskIndex] === "0" && value[valueIndex].match(numberPattern) === null) break;

            // Found a literal
            while (mask[maskIndex].match(literalPattern) === null) {
                if (value[valueIndex] === mask[maskIndex]) break;
                newValue += mask[maskIndex++];
            }
            newValue += value[valueIndex++];
            maskIndex++;
        }

        return input.value = newValue;
    } catch (e) {
        console.log(e);
    }
}

function clear_fields(email, phone) {
    email.value = '';
    phone.value = '';
}

function show_msg( msg ) {
    const parent = document.getElementById('notifications');
    const container = document.createElement('div');
    const container_text = document.createElement('div');
    const container_counter = document.createElement('div');
    let counter = 10;

    container.setAttribute('class', 'notice');

    container_text.setAttribute('class', 'container-text');
    container_text.appendChild( document.createTextNode( msg ) );
    container.appendChild(container_text);

    container_counter.setAttribute('class', 'container-counter');
    container_counter.appendChild( document.createTextNode( counter ) );
    container.appendChild(container_counter);

    backtimer(container, counter);

    parent.appendChild(container);
}

function backtimer( elem, counter ) {

    setTimeout(function() {

        if(counter >= 1) {
            elem.querySelector('.container-counter').innerHTML = counter;
            counter -= 1;
            backtimer(elem, counter);
        } else {
            remove_elem(elem);
        }

    }, 1000);

}

function remove_elem(elem) {
    elem.remove();
}

function ready() {
    if( document.readyState === 'loading' ) {

        document.addEventListener('DOMContentLoaded', ready);

    } else {

        const main_btn = document.getElementById('main_button');
        const popup_el = document.getElementById('popup');
        const close_popup_btn = document.getElementById('close_btn');
        const site_el = document.getElementById('site');
        const email_el = document.getElementById('email');
        const phone_el = document.getElementById('phone-mask');
        const form = document.getElementById('popup_form');

        clear_fields(email_el, phone_el);

        main_btn.addEventListener('click', () => {

            site_el.value = encodeURI(location.origin + '/');
            popup_fn( popup_el, 'open');
        });

        close_popup_btn.addEventListener('click', () => {
            popup_fn( popup_el, 'close');
        });

        phone_el.addEventListener('input', maskInput);

        form.addEventListener('submit', (e) => {

            if(!phone_el.value) {
                e.preventDefault();
                return false;
            }
        });
    }
}

ready();




